__author__ = 'bibro'

import neural_network
import numpy as np
import neurolab as nl

from utils.NetworkHelper import build_input_range


class NeuralNetwork:
    net = None

    def __init__(self, input_size, output_size, max_event_type):
        layers_size = [3, output_size]
        input_range = [0, max_event_type]
        input_range_full = build_input_range(input_range, input_size)

        self.net = nl.net.newff(input_range_full, layers_size)

    def train(self, input, output):
        self.net.train(input, output, show=1)

    def analyze(self, input):
        result = self.net.sim(input)
        return result


if __name__ == "__main__":
    neural_network = NeuralNetwork(5, 3, 3)
    neural_network.train([[1, 0, 1, 0, 2], [0, 0, 1, 2, 3]], [[1, 0, 0], [0, 1, 0]])
    print neural_network.analyze([[0, 0, 1, 2, 3]])
