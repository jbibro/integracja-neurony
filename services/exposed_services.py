from wsgiref.simple_server import make_server
from cornice import Service
from model.NewData import NewData
from model.Person import Person
from model.Result import Result
from model.EventType import EventType
from neural_network import manager
from pyramid.config import Configurator
from pyramid.response import Response
import simplejson as json

info_desc = ""
training = Service(name='training', path='/training')
analytics = Service(name='analytics', path='/analytics')

if __name__ == '__main__':
    config = Configurator()
    # adds cornice
    config.include("cornice")
    config.scan("services.exposed_services")

    app = config.make_wsgi_app()
    server = make_server('0.0.0.0', 65000, app)
    server.serve_forever()

# example INPUT:
# [{"id": 1,
#  "series": [
#		[{"type": "e1", "timestamp": "2014"},{"type": "e1", "timestamp": "2014"}],
#		[{"type": "e1", "timestamp": "2014"},{"type": "e1", "timestamp": "2014"}],
#		...
#			]
# }, ... ]
@training.post()
def hello_world(request):
    obj = request.json_body

    training_data = {}
    if isinstance(obj, list):
        for item in obj:
            if isinstance(item, dict):
                person = Person(item["id"])
                user_series = []
                value = item["series"]
                if isinstance(value, list):
                    series = []
                    for series_items in value:
                        if isinstance(series_items, list):
                            for series_item in series_items:
                                if isinstance(series_item, dict):
                                    data = NewData(series_item["type"], series_item["timestamp"])
                                    series.append(data)
                                else:
                                    raise Exception("Invalid input format. Event is incorrect")
                        else:
                            raise Exception("Invalid input format. User series should contain list of events");
                    user_series.append(series)
                else:
                    raise Exception("Invalid input format. No user series")
                training_data[person] = series
            else:
                raise Exception("Invalid input format. No user objects")
    else:
        raise Exception("Invalid input format. Missing list of users")

    manager.train(training_data)

    return Response("ok")

# example input: [{"type":"e1", "timestamp":"20140101120000"},{"type":"e2", "timestamp":"20140101120002"}]
#
# example output:
@analytics.post()
def hello_world(request):
    obj = request.json_body

    series = []
    if isinstance(obj, list):
        for tpl in obj:
            if isinstance(tpl, dict):
                data = NewData(tpl["type"], tpl["timestamp"])
                series.append(data)
            else:
                raise Exception()
    else:
        raise Exception()

    # launch computation on data
    # series = []
    # series.append([NewData(EventType.e2, 2)])
    # series.append([NewData(EventType.e3, 5)])

    id = manager.analyze(series)

    return serialize(id)


def serialize(obj):
    """Recursively walk object's hierarchy."""
    if isinstance(obj, (bool, int, float)):
        return obj
    elif isinstance(obj, dict):
        obj = obj.copy()
        for key in obj:
            obj[key] = serialize(obj[key])
        return obj
    elif isinstance(obj, list):
        return [serialize(item) for item in obj]
    elif isinstance(obj, tuple):
        return tuple(serialize([item for item in obj]))
    elif hasattr(obj, '__dict__'):
        return serialize(obj.__dict__)
    else:
        return repr(obj) # Don't know how to handle, convert to string
    return json.dumps(serialize(obj))

